<?php

require_once('config.php');

session_start();
$password = $_REQUEST['password'];
$passwordH = password_hash($_REQUEST['password'], PASSWORD_ARGON2ID);
$email = $_REQUEST['email'];
$id = $_SESSION['id'];
$name = $_REQUEST['name'];

echo $name;


try {

    $conn = new PDO("mysql:host=$servername;dbname=$database", $usernamedb, $passworddb);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare("UPDATE users SET name=:name, password= :password, email=:email WHERE id=:id");
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':password', $passwordH);

    $_SESSION['email'] = $email;
    $_SESSION['password'] = $password;
    $_SESSION['name'] = $name;


    $stmt->execute();
    echo $stmt->rowCount();
    header('Location:  http://localhost:8080/view/profile.php');

} catch (PDOException $exception) {
    echo $exception . 'Error';
}


?>
