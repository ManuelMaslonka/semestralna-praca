<?php
session_start();

require_once($_SERVER['DOCUMENT_ROOT'] . "/controler/config.php");

//vlozenie s hashom
try {

    $role = 0;
    $name = $_REQUEST['name'];
    $password = password_hash($_REQUEST['password'], PASSWORD_ARGON2ID);
    $email = $_REQUEST['email'];

    // Connect Database
    $conn = new PDO("mysql:host=$servername;dbname=$database", $usernamedb, $passworddb);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare("SELECT email FROM users");
    $stmt->execute();

    // Verify Email duplicate

    $stmt->setFetchMode(PDO::FETCH_OBJ);
    while ($row = $stmt->fetch()) {
        if ($row->email == $email) {
            $_SESSION['LoginSuccess'] = false;
            header('Location:  http://localhost:8080/view/registration.php');
            die();
        } else {

        }
    }

    // Connect Database
    $conn = new PDO("mysql:host=$servername;dbname=$database", $usernamedb, $passworddb);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $mySql = 'INSERT INTO users (email, password, name, role) VALUES (:email, :password,:name, :role)';
    $stmt = $conn->prepare($mySql);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':password', $password);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':role', $role);


    $stmt->execute();
    $_SESSION['LoginSuccess'] = true;
    header('Location:  http://localhost:8080/view/registration.php');


} catch (PDOException $e) {
    echo 'Chyba' . $e->getMessage();
}


?>
