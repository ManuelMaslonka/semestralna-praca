<?php
require_once 'config.php';

try {
    if (isset($_REQUEST['password']) && isset($_REQUEST['email'])) {
        $password = $_REQUEST['password'];
        $email = $_REQUEST['email'];
        $conn = new PDO("mysql:host=$servername;dbname=$database", $usernamedb, $passworddb);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt = $conn->prepare("SELECT id, email, password, name, role FROM users");
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_OBJ);
        while ($row = $stmt->fetch()) {
            if (password_verify($password, $row->password) && $row->email == $email) {
//                echo 'login ';
                session_start();
//                echo $email;
                $_SESSION['id'] = $row->id;
                $_SESSION['email'] = $row->email;
                $_SESSION['name'] = $row->name;
                $_SESSION['role'] = $row->role;
                $_SESSION['password'] = $password;
                $_SESSION['isLogin'] = true;
                header('Location:  http://localhost:8080/view/home.php');
                die();
            } else {
                session_start();
                $_SESSION['error'] = "nevies heslo alebo meno ";
                header('Location:  http://localhost:8080/view/login.php');
            }
        }
    }
    else {
    }

} catch (PDOException $e) {
    echo "Chyba" . $e->getMessage();
}
?>
