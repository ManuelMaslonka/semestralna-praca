<?php

require_once 'config.php';

try {
    $conn = new PDO("mysql:host=$servername;dbname=$database", $usernamedb, $passworddb);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    $stmt = $conn->prepare("SELECT name, id, price, note, count FROM products");
    $stmt->execute();


    echo '<table class="table" style="background-color: black; color: white">
                                <thead>
                                    <tr>
                                        <th scope="col">Produkt</th>
                                        <th scope="col">Poznamka k produktu</th>
                                        <th scope="col">Cena</th>
                                        <th scope="col">Pocet</th>
                                        <th scope="col">Akcia</th>
                                    </tr>
                                </thead>
                                <tbody>';
    $stmt->setFetchMode(PDO::FETCH_OBJ);

    $buttonEdit = '';
    $buttonAdd = '';




    while ($row = $stmt->fetch()) {
        if (isset($_SESSION['isLogin']) && isset($_SESSION['role'])) {
            if ($_SESSION['isLogin'] == true && $_SESSION['role'] == 1) {
                $buttonEdit = ' <buttun class="btn btn-primary"><a style="text-decoration: unset; color: black" href="editProd.php?idP=' . $row->id . '">Edit</a></buttun>';
                $buttonAdd = '<buttun class="btn btn-danger"><a style="text-decoration: unset; color: black" href="../controler/addProductToUser.php?idP=' . $row->id . '">Add</a></buttun>';
            } elseif ($_SESSION['isLogin'] == true) {
                $buttonAdd = '<buttun class="btn btn-danger"><a style="text-decoration: unset; color: black" href="../controler/addProductToUser.php?idP=' . $row->id . '">Add</a></buttun>';
            }
        }

        echo '<tr>
                                        <td>' . $row->name . '</td>
                                        <td>' . $row->note . '</td>
                                        <td>' . $row->price . '</td>
                                        <td>' . $row->count . '</td>
                                        <td>' . $buttonEdit . $buttonAdd . '
                                        </td>
                                    </tr>
                           ';
    }
    echo '  </tbody>
                            </table>';


} catch (PDOException $e) {
    echo 'Chyba' . $e->getMessage();
}

?>
