<?php

require_once '../controler/config.php';

$idUser = $_REQUEST['idP'];

try {

    $conn = new PDO("mysql:host=$servername;dbname=$database", $usernamedb, $passworddb);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare("SELECT * FROM users WHERE id=:id");
    $stmt->bindParam(':id', $idUser);
    $stmt->execute();

    $stmt->setFetchMode(PDO::FETCH_OBJ);
    while ($row = $stmt->fetch()) {
        $name = $row->name;
        $email = $row->email;
        $products_id = $row->products_id;
        $role = $row->role;
    }

} catch (PDOException $e) {
    echo 'Chyba' . $e->getMessage();
}

?>
