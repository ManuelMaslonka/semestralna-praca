<?php

require_once('config.php');

session_start();
$email = $_REQUEST['email'];
$id = $_REQUEST['id'];
$name = $_REQUEST['name'];
$products_id = $_REQUEST['products'];

echo $id;

try {

    $conn = new PDO("mysql:host=$servername;dbname=$database", $usernamedb, $passworddb);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare("UPDATE users SET name=:name, email=:email, products_id= :products_id WHERE id=:id");
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':products_id', $products_id);
    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':email', $email);

    $stmt->execute();
    echo $stmt->rowCount();
    header('Location:  http://localhost:8080/view/users.php');

} catch (PDOException $exception) {
    echo $exception . 'Error';
}


?>
