<?php

require_once('config.php');

try {

    $price = $_REQUEST['priceP'];
    $name = $_REQUEST['nameP'];
    $count = $_REQUEST['countP'];
    $note = $_REQUEST['noteP'];
    $id = $_REQUEST['idP'];

    echo $id;

    $conn = new PDO("mysql:host=$servername;dbname=$database", $usernamedb, $passworddb);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare("UPDATE products SET name=:name, price= :price, count=:count, note= :note where id=:id");
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':count', $count);
    $stmt->bindParam(':note', $note);
    $stmt->bindParam(':price', $price);

    $stmt->execute();
    echo $stmt->rowCount();
    header('Location:  http://localhost:8080/view/products.php');

} catch (PDOException $e) {
    echo 'Chyba' . $e->getMessage();
}

?>
