<?php

require_once '../controler/config.php';

$idUser = $_REQUEST['idP'];

try {

    $conn = new PDO("mysql:host=$servername;dbname=$database", $usernamedb, $passworddb);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare("DELETE from users WHERE id=:id");
    $stmt->bindParam(':id', $idUser);
    $stmt->execute();

    header('Location:  http://localhost:8080/view/registration.php');

} catch (PDOException $e) {
    echo 'Chyba' . $e->getMessage();
}

?>
