<?php
session_start();

require_once 'config.php';

$idUser = $_SESSION['id'];
$idProd = $_REQUEST['idP'];

try {
    $conn = new PDO("mysql:host=$servername;dbname=$database", $usernamedb, $passworddb);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare("SELECT * FROM users where id=:id");
    $stmt->bindParam(':id', $idUser);
    $stmt->execute();

    $stmt->setFetchMode(PDO::FETCH_OBJ);
    $arrayOfProducts[] = [];
    while ($row = $stmt->fetch()) {
        $arrayOfProducts = explode(" ",$row->products_id);
    }

    $conn = new PDO("mysql:host=$servername;dbname=$database", $usernamedb, $passworddb);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    array_push($arrayOfProducts, $idProd);
    $stringOfProducts = implode(' ', $arrayOfProducts);

    $stmt = $conn->prepare('UPDATE users SET products_id=:products_id WHERE id=:id');
    $stmt->bindParam(':id', $idUser);
    $stmt->bindParam('products_id', $stringOfProducts);

    $stmt->execute();

    header('Location:  http://localhost:8080/view/products.php');

} catch (PDOException $e) {
    echo 'Chyba' . $e->getMessage();
}


?>
