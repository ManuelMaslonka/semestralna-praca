<?php

require_once 'config.php';

$id = $_REQUEST['idP'];
$nameP = '';
$priceP = '';
$countP = '';
$noteP = '';

try {
    $conn = new PDO("mysql:host=$servername;dbname=$database", $usernamedb, $passworddb);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    $stmt = $conn->prepare("SELECT name, id, price, note, count FROM products where id=:id");
    $stmt->bindParam(':id', $id);
    $stmt->execute();


    $stmt->setFetchMode(PDO::FETCH_OBJ);
    while ($row = $stmt->fetch()) {
        $nameP = $row->name;
        $priceP = $row->price;
        $countP = $row->count;
        $noteP = $row->note;
    }

} catch (PDOException $e) {
    echo 'Chyba' . $e->getMessage();
}

?>
