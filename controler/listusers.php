<?php

require_once 'config.php';

try {
    $conn = new PDO("mysql:host=$servername;dbname=$database", $usernamedb, $passworddb);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    $stmt = $conn->prepare("SELECT name, id, email, role, products_id FROM users");
    $stmt->execute();


    echo '<table class="table" style="background-color: black; color: white">
                                <thead>
                                    <tr>
                                        <th scope="col">Id</th>
                                        <th scope="col">Meno</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Rola</th>
                                        <th scope="col">Products_id</th>
                                        <th scope="col">Akcia</th>
                                    </tr>
                                </thead>
                                <tbody>';
    $stmt->setFetchMode(PDO::FETCH_OBJ);


    while ($row = $stmt->fetch()) {
        if (isset($_SESSION['id'])) {
            if ($_SESSION['id'] == $row->id){
                continue;
            }
        }
        echo '<tr>
                                        <td>' . $row->id . '</td>
                                        <td>' . $row->name . '</td>
                                        <td>' . $row->email . '</td>
                                        <td>' . $row->role . '</td>
                                        <td>' . $row->products_id . '</td>
                                        <td><buttun class="btn btn-primary"><a style="text-decoration: unset; color: black" href="editUser.php?idP=' . $row->id . '">Edit</a></buttun>
                                        <buttun class="btn btn-danger"><a style="text-decoration: unset; color: black" href="../controler/deleteUser.php?idP=' . $row->id . '">Delete</a></buttun></td>
                                    </tr>
                           ';
    }
    echo '  </tbody>
                            </table>';


} catch (PDOException $e) {
    echo 'Chyba' . $e->getMessage();
}

?>
