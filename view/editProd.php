<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible"
              content="ie=edge">
        <title>Document</title>
        <link rel="icon"
              type="image/x-icon"
              href="../assets/favicon.ico"/>
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js"
                crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Varela+Round"
              rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
              rel="stylesheet"/>
        <script src="https://code.jquery.com/jquery-3.6.4.min.js"
                integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8="
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"
                integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA=="
                crossorigin="anonymous"
                referrerpolicy="no-referrer"></script>
        <link href="../css/styles.css"
              rel="stylesheet"/>
        <link rel="stylesheet"
              href="../css/main.css"/>
    </head>
    <body>
        <body id="page-top">
            <nav class="navbar navbar-expand-lg navbar-light fixed-top"
                 id="mainNav">
                <div class="container px-4 px-lg-5">
                    <a class="navbar-brand"
                       href="home.php">UNI CHAT</a>
                    <button class="navbar-toggler navbar-toggler-right"
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#navbarResponsive"
                            aria-controls="navbarResponsive"
                            aria-expanded="false"
                            aria-label="Toggle navigation">
                        Menu
                        <i class="fas fa-bars"></i>
                    </button>
                    <div class="collapse navbar-collapse"
                         id="navbarResponsive">
                        <ul class="navbar-nav ms-auto">
                            <?php
                            session_start();

                            require_once($_SERVER['DOCUMENT_ROOT'] . '/controler/verifyIslogin.php');


                            ?>
                            <?= $elementL ?>
                            <?= $elementR ?>
                            <?= $elementK ?>



                            <?=$elementProfile?>
                            <?= $elementA ?>
                            <?= $elementP ?>
                            <?= $elementO ?>
                            <?= $elementU ?>

                        </ul>
                    </div>
                </div>
            </nav>

            <?php
            require_once '../controler/getProduct.php';
            $idP = $_REQUEST['idP'];
            ?>

            <header class="masthead">
                <div class="container px-4 px-lg-5 d-flex h-100 align-items-center justify-content-center">
                    <div class="d-flex justify-content-center">
                        <div class="text-center">
                            <h3>Edit Product</h3>
                            <form action="../controler/updateProducts.php"
                                  method="post"
                                  id="editProd">
                                <div class="mb-3">
                                    <label for="nameP"
                                           class="form-label">Name</label>
                                    <input type="text"
                                           class="form-control"
                                           id="nameP"
                                           name="nameP"
                                           value=<?= $nameP ?>
                                    >
                                </div>
                                <div class="mb-3">
                                    <label for="noteP"
                                           class="form-label">Note</label>
                                    <input type="text"
                                           class="form-control"
                                           id="noteP"
                                           name="noteP"
                                           value=<?= $noteP ?>
                                    >
                                </div>
                                <div class="mb-3">
                                    <label for="priceP"
                                           class="form-label">Price</label>
                                    <input type="number"
                                           class="form-control"
                                           id="priceP"
                                           name="priceP"
                                           value=<?= $priceP ?>
                                    >
                                </div>
                                <div class="mb-3">
                                    <label for="countP"
                                           class="form-label">Count</label>
                                    <input type="number"
                                           class="form-control"
                                           id="countP"
                                           name="countP"
                                           value=<?= $countP ?>
                                    >
                                </div>
                                <input type="hidden"
                                       name="idP"
                                       value=<?= $idP ?>>
                                <button type="submit"
                                        class="btn btn-primary">Update
                                </button>
                        </div>
                    </div>
                </div>
            </header>

            <script>
                $('#editProd').validate({
                    rules: {
                        noteP: {
                            required: true
                        },
                        nameP: {
                            required: true
                        },
                        countP: {
                            required: true
                        },
                        priceP: {
                            required: true
                        }
                    }
                })
            </script>

            <!-- Contact-->
            <!-- Footer-->
            <footer class="footer bg-black small text-center text-white-50">
                <div class="container px-4 px-lg-5">Copyright &copy; Maslonka 2023</div>
            </footer>

            <!-- Bootstrap core JS-->
            <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
                    integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3"
                    crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
            <script src="../js/scripts.js"></script>
            <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
            <script src="../js/jqlocalization.js"></script>
        </body>
</html>
