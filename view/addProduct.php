<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible"
              content="ie=edge">
        <title>Document</title>
        <link rel="icon"
              type="image/x-icon"
              href="../assets/favicon.ico"/>
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js"
                crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Varela+Round"
              rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
              rel="stylesheet"/>
        <script src="https://code.jquery.com/jquery-3.6.4.min.js"
                integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8="
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"
                integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA=="
                crossorigin="anonymous"
                referrerpolicy="no-referrer"></script>
        <link href="../css/styles.css"
              rel="stylesheet"/>
        <link rel="stylesheet"
              href="../css/main.css"/>
    </head>
    <body>
        <body id="page-top">
            <nav class="navbar navbar-expand-lg navbar-light fixed-top"
                 id="mainNav">
                <div class="container px-4 px-lg-5">
                    <a class="navbar-brand"
                       href="home.php">UNI CHAT</a>
                    <button class="navbar-toggler navbar-toggler-right"
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#navbarResponsive"
                            aria-controls="navbarResponsive"
                            aria-expanded="false"
                            aria-label="Toggle navigation">
                        Menu
                        <i class="fas fa-bars"></i>
                    </button>
                    <div class="collapse navbar-collapse"
                         id="navbarResponsive">
                        <ul class="navbar-nav ms-auto">
                            <?php
                            session_start();

                            require_once($_SERVER['DOCUMENT_ROOT'] . '/controler/verifyIslogin.php');


                            ?>
                            <?= $elementL ?>
                            <?= $elementR ?>
                            <?= $elementK ?>



                            <?= $elementProfile ?>
                            <?= $elementA ?>
                            <?= $elementP ?>
                            <?= $elementO ?>
                            <?= $elementU ?>

                        </ul>
                    </div>
                </div>
            </nav>
            <header class="masthead">
                <div class="container px-4 px-lg-5 d-flex h-100 align-items-center justify-content-center">
                    <div class="d-flex justify-content-center">
                        <div class="text-center">
                            <h3>Pridanie produktu</h3>
                            <form action="../controler/addProducts.php"
                                  method="post"
                                  id="addProducts">
                                <div class="mb-3">
                                    <label for="name"
                                           class="form-label">Meno produktu</label>
                                    <input type="text"
                                           class="form-control"
                                           id="name"
                                           name="name"
                                           aria-describedby="emailHelp">
                                </div>
                                <div class="mb-3">
                                    <label for="note"
                                           class="form-label">Popis Produktu</label>
                                    <input
                                        type="text"
                                        id="note"
                                        name="note"
                                        class="form-control"
                                    >


                                </div>
                                <div class="mb-3">
                                    <label for="count"
                                           class="form-label">Pocet</label>
                                    <input
                                        type="number"
                                        id="count"
                                        name="count"
                                        class="form-control"
                                    >

                                </div>
                                <div class="mb-3">
                                    <label for="price"
                                           class="form-label">Cena</label>
                                    <input
                                        type="number"
                                        id="price"
                                        name="price"
                                        class="form-control"
                                    >

                                </div>
                                <button type="submit"
                                        class="btn btn-primary">Submit
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </header>

            <script>
                $('#addProducts').validate({
                    rules: {
                        note: {
                            required: true
                        },
                        name: {
                            required: true
                        },
                        count: {
                            required: true
                        },
                        price: {
                            required: true
                        }
                    }
                })
            </script>


            <!-- Contact-->
            <!-- Footer-->
            <footer class="footer bg-black small text-center text-white-50">
                <div class="container px-4 px-lg-5">Copyright &copy; Maslonka 2023</div>
            </footer>

            <!-- Bootstrap core JS-->
            <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
                    integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3"
                    crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
            <script src="../js/scripts.js"></script>
            <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
            <script src="../js/jqlocalization.js"></script>


        </body>
</html>
